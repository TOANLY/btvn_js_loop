// Bài 1: Tìm số nguyên dương nhỏ nhất 
// var soNguyenDuong = 0;
tinhTongSoNguyen = 0;
for (a=0; a < 10000 ; a++) {
    tinhTongSoNguyen += a ;
    return tinhTongSoNguyen > 10000;
}
var soNguyenDuongNhoNhat = Math.min(tinhTongSoNguyen);
document.getElementById('soNguyenDuongNhoNhat').innerHTML = `Số nguyên dương nhỏ nhất là ${soNguyenDuongNhoNhat}`;
// bài 2 : tính tổng n 
function tinhTong() {
    // input
    var x = document.getElementById('soX').value*1;
    var n = document.getElementById('soN').value*1;
    // output 
    tong = 0;

    // xử lý hàm 
    
    for (var s=1; s <= n; s++) {
        var ketQua = Math.pow(x,s);
        var tong = tong + ketQua;
    }
    document.querySelector('#tinhTong').innerHTML = `tổng là ${tong}`;
}

//bài 3: Tính giai thừa 
function tinhGiaiThua() {
    //input
    var soN = document.querySelector('#nhapVaon').value*1;
    //output
    var giaiThua = 1;

    //xử lý hàm 
    for (var giaTri = 1; giaTri <= soN; giaTri++) {
        giaiThua = giaiThua * giaTri ;
    }
    document.querySelector('#giaiThua').innerHTML = `giai thừa là ${giaiThua}`;
}

// bài 4: thẻ div xanh đỏ 
function divXanhDo () {
    //input
    content = '';
    container = '';
    // xử lý 
    for (i=1; i<= 10; i++) {
        if (i % 2 == 0) {
            content = `<p style="background-color: red;"> Số ${i} div chẵn  </p>`;
        } else {
            content = `<p style="background-color: blue;"> Số ${i} div lẻ  </p>`;
        }
        container = container + content ; 
    }
    // kết quả 
    document.querySelector('#divXanhDo').innerHTML = container;
}
